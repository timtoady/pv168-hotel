CREATE TABLE guest (
    id BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    cardid VARCHAR(127) NOT NULL
);

CREATE TABLE room (
    id BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    floor INTEGER NOT NULL,
    roomNumber INTEGER NOT NULL,
    capacity INTEGER NOT NULL
);

CREATE TABLE accommodation (
    id BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    guestid BIGINT REFERENCES guest (id),
    roomid BIGINT REFERENCES room (id),
    startDate DATE NOT NULL,
    endDate DATE
);

INSERT INTO ROOM (floor, roomNumber, capacity) VALUES (1,1,3);
INSERT INTO ROOM (floor, roomNumber, capacity) VALUES (2,2,4);
INSERT INTO ROOM (floor, roomNumber, capacity) VALUES (3,3,5);
INSERT INTO ROOM (floor, roomNumber, capacity) VALUES (4,4,3);
INSERT INTO ROOM (floor, roomNumber, capacity) VALUES (5,5,2);
INSERT INTO ROOM (floor, roomNumber, capacity) VALUES (6,6,1);

INSERT INTO GUEST (name, address, cardid) VALUES ('A','A','A');
INSERT INTO GUEST (name, address, cardid) VALUES ('B','B','B');
INSERT INTO GUEST (name, address, cardid) VALUES ('C','C','C');
INSERT INTO GUEST (name, address, cardid) VALUES ('D','D','D');
INSERT INTO GUEST (name, address, cardid) VALUES ('E','E','E');
INSERT INTO GUEST (name, address, cardid) VALUES ('F','F','F');
INSERT INTO GUEST (name, address, cardid) VALUES ('G','G','G');
INSERT INTO GUEST (name, address, cardid) VALUES ('H','H','H');
INSERT INTO GUEST (name, address, cardid) VALUES ('I','I','I');
INSERT INTO GUEST (name, address, cardid) VALUES ('J','J','J');
INSERT INTO GUEST (name, address, cardid) VALUES ('K','K','K');
INSERT INTO GUEST (name, address, cardid) VALUES ('L','L','L');
INSERT INTO GUEST (name, address, cardid) VALUES ('M','M','M');
INSERT INTO GUEST (name, address, cardid) VALUES ('N','N','N');

insert into accommodation (guestid, roomid, startDate,endDate) VALUES (2,1,CURRENT_DATE, CURRENT_DATE);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (2,1,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (1,1,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (3,2,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (4,1,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (5,3,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (6,2,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (7,3,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (8,4,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (9,2,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (10,3,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (11,1,CURRENT_DATE, null);
insert into accommodation (guestid, roomid, startDate,endDate) VALUES (12,4,CURRENT_DATE, null);

-- ma pokoj misto?
SELECT capacity, COUNT(roomid) as guestCount
FROM room LEFT JOIN accommodation ON room.id=roomid
WHERE endDate IS NULL AND room.id = 3
GROUP BY room.id,capacity;

-- je host ubytovany? resp. na kolika pokojich je host uz ubytovany
SELECT COUNT(guestid) AS roomCount FROM accommodation
WHERE endDate IS NULL AND guestid = 11;

-- checkin, tj. vlozeni do accommodation
INSERT INTO accommodation (guestid, roomid, startDate) VALUES (2,3,CURRENT_DATE);

-- checkin, pokud je v pokoji misto
-- insert into accommodation (guestid, roomid, startDate)
-- select 13,1,CURRENT_DATE from accommodation
-- where (select count(*) from accommodation where guestid=13 and enddate is null) < 1
-- FETCH FIRST ROW ONLY;

-- checkout, jen se nastavi endDate
UPDATE accommodation SET endDate = CURRENT_DATE WHERE guestid = 11 AND endDate IS NULL;

-- najiti pokoje s hostem
SELECT room.id, floor, roomNumber, capacity
FROM room LEFT JOIN accommodation ON room.id=roomid
WHERE endDate IS NULL AND guestid = 12;

-- hosti v pokoji
SELECT guest.id, name, address, cardid
FROM guest JOIN accommodation ON guest.id=guestid
WHERE endDate IS NULL AND roomid = 3;

-- najiti prazdnych pokoju
SELECT room.id, floor, roomNumber, capacity
FROM room LEFT JOIN accommodation ON room.id=roomid
WHERE endDate IS NULL
GROUP BY room.id, floor, roomNumber, capacity
HAVING COUNT(roomid) = 0;

-- najiti pokoju s mistem
SELECT room.id, floor, roomNumber, capacity
FROM room LEFT JOIN accommodation ON room.id=roomid
WHERE endDate IS NULL
GROUP BY room.id, floor, roomNumber, capacity
HAVING COUNT(roomid) < capacity;


drop table accommodation;
drop table room;
drop table guest;
