package cz.muni.fi.pv168.hotel.backend;

import cz.muni.fi.pv168.common.DBUtils;
import cz.muni.fi.pv168.common.IllegalEntityException;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mik, sahib
 */
public class HotelManagerImplTest {
    
    private HotelManagerImpl manager;
    private RoomManagerImpl roomManager;
    private GuestManagerImpl guestManager;
    private DataSource dataSource;
    
    @Before
    public void setUp() throws SQLException {
        dataSource = prepareDataSource();
        DBUtils.executeSqlScript(dataSource, 
                HotelManager.class.getResource("createTables.sql"));
        manager = new HotelManagerImpl(dataSource);
        roomManager = new RoomManagerImpl(dataSource);
        guestManager= new GuestManagerImpl(dataSource);
    }
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(dataSource, 
                HotelManager.class.getResource("dropTables.sql"));
    }
    
    private static DataSource prepareDataSource() throws SQLException {
        EmbeddedDataSource ds = new EmbeddedDataSource();
        ds.setDatabaseName("memory:hotelmgr-test");
        ds.setCreateDatabase("create");
        return ds;
    }

    private Guest createAndSetGuest(String name, String address, 
            String idCard) {
        Guest result = new Guest();
        result.setName(name);
        result.setAddress(address);
        result.setIdCard(idCard);
        guestManager.createGuest(result);
        
        return guestManager.getGuest(result.getId());
    }
    
    private Room createAndSetRoom(int floor, int roomNumber, 
            int capacity) {
        Room result = new Room();
        result.setFloor(floor);
        result.setRoomNumber(roomNumber);
        result.setCapacity(capacity);
        roomManager.createRoom(result);
        
        return roomManager.getRoom(result.getId());
    }
    
    /**
     * Test of checkInGuest method, of class HotelManagerImpl.
     */
    @Test
    public void testCheckInGuest() {
        Guest guest = createAndSetGuest("Jan Novak", 
                "Certain street 123, Certain town", "bflmpsvz2016");
        Room room = createAndSetRoom(1, 2, 1);
        try {
            manager.checkInGuest(new Guest(), room);
            fail("null guest id not detected");
        } catch (IllegalArgumentException iae) {
            // OK
        }
        

        try {
            manager.checkInGuest(guest, new Room());
            fail("null room id not detected");
        } catch (IllegalArgumentException iae) {
            // OK
        }
        
        assertEquals("no guest should be checked in", 
                manager.listGuestsInRoom(room).size(), 0);
        Guest guest2 = createAndSetGuest("Franta Novacek", 
                "Certain street 321, Certain town", "aeiou2016");
        manager.checkInGuest(guest, room);
        assertEquals("guest was not checked in", 
                manager.listGuestsInRoom(room).size(), 1);
        assertTrue("guest was not checked in (but some other guest was)", 
                manager.listGuestsInRoom(room).contains(guest));
        
        Room room2 = createAndSetRoom(3, 5, 4);
        try {
            manager.checkInGuest(guest, room2);
            fail("guest should not be checked in more rooms");
        } catch (IllegalEntityException iee) {
            // OK
        }
        
        try {
            manager.checkInGuest(guest2, room);
            fail("two guests cannot be checked in to room with capacity one");
        } catch (IllegalEntityException iee) {
            // OK
        }
        
    }

    /**
     * Test of checkOutGuest method, of class HotelManagerImpl.
     */
    @Test
    public void testCheckOutGuest() {
        Guest guest = createAndSetGuest("Jan Novak", 
                "Certain street 123, Certain town", "bflmpsvz2016");
        try {
            manager.checkOutGuest(new Guest());
            fail("null guest id not detected");
        } catch (IllegalArgumentException iae) {
            // OK
        }
        
        Room room = createAndSetRoom(1, 2, 4);
        Guest guest2 = createAndSetGuest("Franta Novacek", 
                "Certain street 321, Certain town", "aeiou2016");
        manager.checkInGuest(guest, room);
        manager.checkInGuest(guest2, room);
        assertEquals("there should be two guests", 
                manager.listGuestsInRoom(room).size(), 2);
        manager.checkOutGuest(guest2);
        assertEquals("there should be one guest in room", 
                manager.listGuestsInRoom(room).size(), 1);
        assertTrue("wrong remaining guest in room", 
                manager.listGuestsInRoom(room).contains(guest));
        manager.checkOutGuest(guest);
        assertEquals("guest was not checked out", 
                manager.findRoomWithGuest(guest), null);
        assertEquals("room should be empty", 
                manager.listGuestsInRoom(room).size(), 0);
        
    }

    /**
     * Test of findRoomWithGuest method, of class HotelManagerImpl.
     */
    @Test
    public void testFindRoomWithGuest() {
        Guest guest = createAndSetGuest("Jan Novak", 
                "Certain street 123, Certain town", "bflmpsvz2016");
        try {
            manager.findRoomWithGuest(new Guest());
            fail("null guest id not detected");
        } catch (IllegalArgumentException iae) {
            // OK
        }

	Guest guest2 = createAndSetGuest("Franta Novacek", 
                "Certain street 321, Certain town", "aeiou2016");
	Guest guest3 = createAndSetGuest("Lenka Novotna", 
                "Some other street 456, Some village", "2016hchkrdtn");
	Room room = createAndSetRoom(1, 2, 2);
	Room room2 = createAndSetRoom(4, 5, 6);

	manager.checkInGuest(guest, room);
	manager.checkInGuest(guest2, room);
	manager.checkInGuest(guest3, room2);
	assertEquals("guest's room not found", 
                manager.findRoomWithGuest(guest), room);
	assertEquals("guest's room not found", 
                manager.findRoomWithGuest(guest2), room);
	assertEquals("guest's room not found", 
                manager.findRoomWithGuest(guest3), room2);

	manager.checkOutGuest(guest2);
	assertEquals("guest was checked out, room should not be found", 
                manager.findRoomWithGuest(guest2), null);
	assertEquals("other guest's room changed during checkout", 
                manager.findRoomWithGuest(guest), room);
    }

    /**
     * Test of listGuestsInRoom method, of class HotelManagerImpl.
     */
    @Test
    public void testListGuestsInRoom() {
        try {
            manager.listGuestsInRoom(new Room());
            fail("null guest id not detected");
        } catch (IllegalArgumentException iae) {
            // OK
        }
        
        Room room = createAndSetRoom(1, 2, 2);
        assertEquals("room should be empty", 
                manager.listGuestsInRoom(room).size(), 0);
        Guest guest = createAndSetGuest("Jan Novak", 
                "Certain street 123, Certain town", "bflmpsvz2016");
        Guest guest2 = createAndSetGuest("Franta Novacek", 
                "Certain street 321, Certain town", "aeiou2016");
        
        manager.checkInGuest(guest, room);
        manager.checkInGuest(guest2, room);
        assertEquals("room should have two guests", 
                manager.listGuestsInRoom(room).size(), 2);
        manager.checkOutGuest(guest2);
        assertEquals("room should have one guest", 
                manager.listGuestsInRoom(room).size(), 1);
        assertTrue("room should contain a guest", 
                manager.listGuestsInRoom(room).contains(guest));
    }

    /**
     * Test of listEmptyRooms method, of class HotelManagerImpl.
     */
    @Test
    public void testListEmptyRooms() {
        assertTrue("no (empty or other) rooms should be found", 
                manager.listEmptyRooms().isEmpty());
        Room room = createAndSetRoom(1, 4, 10);
        assertEquals("one empty room should be found", 
                manager.listEmptyRooms().size(), 1);
        
        Room room2 = createAndSetRoom(2, 3, 8);
        assertTrue("new empty room should not change previous empty room", 
                manager.listEmptyRooms().contains(room));
        assertEquals("two empty rooms should be found", 
                manager.listEmptyRooms().size(), 2);
        
        Guest guest = createAndSetGuest("Jan Novak", 
                "Certain street 123, Certain town", "bflmpsvz2016");
        manager.checkInGuest(guest, room2);
        assertEquals("one empty room should be found", 
                manager.listEmptyRooms().size(), 1);
        assertTrue("new check in elsewhere should not change other empty room", 
                manager.listEmptyRooms().contains(room));
    }

    /**
     * Test of listRoomsWithSomeFreeSpace method, of class HotelManagerImpl.
     */
    @Test
    public void testListRoomsWithSomeFreeSpace() {
        assertTrue("no (with free space or other) rooms should be found", 
                manager.listRoomsWithSomeFreeSpace().isEmpty());
        Room room = createAndSetRoom(1, 4, 1);
        assertEquals("one room with some free space should be found", 
                manager.listRoomsWithSomeFreeSpace().size(), 1);
        
        Room room2 = createAndSetRoom(2, 6, 1);
        assertEquals("two rooms with some free space should be found", 
                manager.listRoomsWithSomeFreeSpace().size(), 2);
        Guest guest = createAndSetGuest("Jan Novak", 
                "Certain street 123, Certain town", "bflmpsvz2016");
        manager.checkInGuest(guest, room);
        assertEquals("one room with some free space should be found", 
                manager.listRoomsWithSomeFreeSpace().size(), 1);
        assertTrue("room with some free space was changed during check in "
                + "elsewhere", 
                manager.listRoomsWithSomeFreeSpace().contains(room2));
        
        Guest guest2 = createAndSetGuest("Franta Novacek", 
                "Certain street 321, Certain town", "aeiou2016");
        manager.checkInGuest(guest2, room2);
        assertTrue("no room with free space should be found", 
                manager.listRoomsWithSomeFreeSpace().isEmpty());
    }
    
}
