package cz.muni.fi.pv168.hotel.backend;

import cz.muni.fi.pv168.common.DBUtils;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;
import org.apache.derby.jdbc.EmbeddedDataSource;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sahib
 */
public class GuestManagerImplTest {

    private GuestManager manager;
    private DataSource dataSource;

    @Before
    public void setUp() throws SQLException {
        dataSource = prepareDataSource();
        DBUtils.executeSqlScript(dataSource,GuestManager.class.getResource("createTables.sql"));
        manager = new GuestManagerImpl(dataSource);
    }
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(dataSource,GuestManager.class.getResource("dropTables.sql"));
    }
    
    private static DataSource prepareDataSource() throws SQLException {
        EmbeddedDataSource ds = new EmbeddedDataSource();
        ds.setDatabaseName("memory:guestmgr-test");
        ds.setCreateDatabase("create");
        return ds;
    }

    /**
     * Test of createGuest method, of class GuestManagerImpl.
     */
    @Test
    public void testCreateGuest() {
//        System.out.println("createGuest");
        Guest guest = new Guest("Jan", "Ulice", "xxx-xxx-xxx");
        manager.createGuest(guest);
        assertThat("saving guest failed, guest id null", guest.getId(), is(not(equalTo(null))));
    }

    /**
     * Test of createGuest method, of class GuestManagerImpl.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNull() throws Exception {
//        System.out.println("createGuestWithNull");
        manager.createGuest(null);
    }

    /**
     * Test of createGuest method, of class GuestManagerImpl.
     */
    @Test
    public void testCreateGuestWithWrongValues() {
//        System.out.println("createGuestWithWrongValues");
        Guest guest = new Guest("Honza", "Adresa", "1xx-xxx-xxx");
        manager.createGuest(guest);
        guest.setId(1L);
        try {
            manager.createGuest(guest);
            fail("should refuse assigned id");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        guest = new Guest(null, "Adresa", "1xx-xxx-xxx");
        try {
            manager.createGuest(guest);
            fail("null name not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        guest = new Guest("Honza", null, "1xx-xxx-xxx");
        try {
            manager.createGuest(guest);
            fail("null address not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        guest = new Guest("Honza", "Adresa", null);
        try {
            manager.createGuest(guest);
            fail("null idCard not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

    }

    /**
     * Test of updateGuest method, of class GuestManagerImpl.
     */
    @Test
    public void testUpdateGuest() {
//        System.out.println("updateGuest");
        Guest guest = new Guest("Honza", "Adresa", "1xx-xxx-xxx");
        Guest g2 = new Guest("Jean", "Address", "xx7-xxx-xxx");
        manager.createGuest(guest);
        Long guestId = guest.getId();
        manager.createGuest(g2);
        guest.setAddress("New Address");
        manager.updateGuest(guest);
        Guest result = manager.getGuest(guestId);
        assertThat("name changed while changing address", result.getName(), is(equalTo("Honza")));
        assertThat("address isn't changed", result.getAddress(), is(equalTo("New Address")));
        assertThat("cardId changed while changing address", result.getIdCard(), is(equalTo("1xx-xxx-xxx")));

        assertThat("other record changed during update", manager.getGuest(g2.getId()), is(equalTo(g2)));
//        doplint jako v GuestManagerImplTest?
    }

    /**
     * Test of updateGuest method, of class GuestManagerImpl.
     */
    @Test
    public void testUpdateGuestWithWrongValues() {
//        System.out.println("updateGuestWithWrongValues");
        Guest guest = new Guest("Little Bobby", "Tables 16, Oxfordshire", "DEADBEEFBAADF00D");
        manager.createGuest(guest);
        Long guestId = guest.getId();

        try {
            manager.updateGuest(null);
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            guest = manager.getGuest(guestId);
            guest.setId(null);
            manager.updateGuest(guest);
            fail("null id not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            guest = manager.getGuest(guestId);
            guest.setName(null);
            manager.updateGuest(guest);
            fail("null name not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            guest = manager.getGuest(guestId);
            guest.setAddress(null);
            manager.updateGuest(guest);
            fail("null address not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            guest = manager.getGuest(guestId);
            guest.setIdCard(null);
            manager.updateGuest(guest);
            fail("null cardId not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

    }

    /**
     * Test of deleteGuest method, of class GuestManagerImpl.
     */
    @Test
    public void testDeleteGuest() {
//        System.out.println("deleteGuest");
        Guest g1 = new Guest("Jay", "adr 1", "BEC00L");
        Guest g2 = new Guest("Silent Bob", "adr 2", "2C00L2");
        manager.createGuest(g1);
        manager.createGuest(g2);

        assertNotNull(manager.getGuest(g1.getId()));
        assertNotNull(manager.getGuest(g2.getId()));

        manager.deleteGuest(g1);

        assertNull(manager.getGuest(g1.getId()));
        assertNotNull(manager.getGuest(g2.getId()));
    }

    /**
     * Test of getGuest method, of class GuestManagerImpl.
     */
    @Test
    public void testGetGuest() {
//        System.out.println("getGuest");
        Guest guest = new Guest("Mama Bear", "3 Bears' House, In da Woods", "genuine bear certificate #2");
        manager.createGuest(guest);
        Long guestId = guest.getId();
        Guest result = manager.getGuest(guestId);
        assertThat("retrieved guest is null", result, is(not(equalTo(null))));
        assertThat("retrieved guest differs from the saved one", result, is(equalTo(guest)));
        assertThat("retrieved room is the same instance", result, is(not(sameInstance(guest))));
    }

    /**
     * Test of listAllGuests method, of class GuestManagerImpl.
     */
    @Test
    public void testListAllGuests() {
//        System.out.println("listAllGuests");
        assertTrue(manager.listAllGuests().isEmpty());

        Guest g1 = new Guest("Papa Bear", "3 Bears' House, In da Woods", "genuine bear certificate #1");
        Guest g2 = new Guest("Baby Bear", "3 Bears' House, In da Woods", "genuine bear certificate #3");

        manager.createGuest(g1);
        manager.createGuest(g2);

        Set<Guest> expected = new HashSet<>(Arrays.asList(g1, g2));
        Set<Guest> actual = new HashSet<>(manager.listAllGuests());

        assertEquals("saved and retrieved rooms differ", expected, actual);
    }
    
}
