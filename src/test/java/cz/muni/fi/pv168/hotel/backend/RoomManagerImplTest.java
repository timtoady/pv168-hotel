package cz.muni.fi.pv168.hotel.backend;

import cz.muni.fi.pv168.common.DBUtils;
import java.sql.SQLException;
import org.apache.derby.jdbc.EmbeddedDataSource;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 *
 * @author sahib, mik
 */
public class RoomManagerImplTest {

    private RoomManagerImpl manager;
    private DataSource dataSource;

    @Before
    public void setUp() throws SQLException {
        dataSource = prepareDataSource();
        DBUtils.executeSqlScript(dataSource,
                GuestManager.class.getResource("createTables.sql"));// nema byt zde misto GuestManager RoomManager?
        manager = new RoomManagerImpl(dataSource);
    }
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(dataSource,
                GuestManager.class.getResource("dropTables.sql"));// nema byt zde misto GuestManager RoomManager?
    }
    
    private static DataSource prepareDataSource() throws SQLException {
        EmbeddedDataSource ds = new EmbeddedDataSource();
        ds.setDatabaseName("memory:roommgr-test");
        ds.setCreateDatabase("create");
        return ds;
    }

    /**
     * Test of createRoom and getRoom methods, of class RoomManagerImpl.
     */
    @Test
    public void testCreateAndGetRoom() {
        Room room = createAndSetRoom(2, 15, 4);
        manager.createRoom(room);
        Long roomId = room.getId();
        assertThat("saved room has null id", roomId, is(not(equalTo(null))));
        Room result = manager.getRoom(roomId);
        assertThat("retrieved room is null", result, is(not(equalTo(null))));
        assertThat("retrieved room differs from the saved one", result, 
                is(equalTo(room)));
        assertThat("retrieved room is the same instance", result, 
                is(not(sameInstance(room))));
    }

    /**
     * Test of createRoom method, of class RoomManagerImpl.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNull() throws Exception {
        manager.createRoom(null);
    }

    /**
     * Test of createRoom method, of class RoomManagerImpl.
     */
    @Test
    public void createRoomWithWrongValues() {
        Room room = createAndSetRoom(12, 13, 6);
        room.setId(1L);
        try {
            manager.createRoom(room);
            fail("should refuse assigned id");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        room = createAndSetRoom(-1, 13, 6);
        try {
            manager.createRoom(room);
            fail("negative floor number not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        room = createAndSetRoom(1, -1, 6);
        try {
            manager.createRoom(room);
            fail("negative roomNumber not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        room = createAndSetRoom(1, 0, 6);
        try {
            manager.createRoom(room);
            fail("zero roomNumber not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        room = createAndSetRoom(1, 1, -1);
        try {
            manager.createRoom(room);
            fail("negative capacity not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        room = createAndSetRoom(1, 1, 0);
        try {
            manager.createRoom(room);
            fail("zero capacity not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

    }

    /**
     * Test of updateRoom method, of class RoomManagerImpl.
     */
    @Test
    public void testUpdateRoom() {
        Room room = createAndSetRoom(7, 18, 4);
        Room r2 = createAndSetRoom(2, 3, 4);
        manager.createRoom(room);
        Long roomId = room.getId();
        manager.createRoom(r2);
        room.setCapacity(5);
        manager.updateRoom(room);
        Room result = manager.getRoom(roomId);
        assertThat("roomNumber changed while changing capacity",
                result.getRoomNumber(), is(equalTo(18)));
        assertThat("capacity isn't changed", result.getCapacity(),
                is(equalTo(5)));
        assertThat("roomNumber changed while changing capacity",
                result.getFloor(), is(equalTo(7)));

        assertThat("other record changed during update",
                manager.getRoom(r2.getId()), is(equalTo(r2)));
    }

    /**
     * Test of updateRoom method, of class RoomManagerImpl.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWithNull() throws Exception {
        manager.updateRoom(null);
    }

    /**
     * Test of createRoom method, of class RoomManagerImpl.
     */
    @Test
    public void testUpdateRoomWithWrongValues() {
        Room room = createAndSetRoom(12, 13, 6);
        manager.createRoom(room);
        Long roomId = room.getId();

        try {
            room = manager.getRoom(roomId);
            room.setId(null);
            manager.updateRoom(room);
            fail("null id not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            room = manager.getRoom(roomId);
            room.setFloor(-1);
            manager.updateRoom(room);
            fail("negative floor number not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            room = manager.getRoom(roomId);
            room.setRoomNumber(-1);
            manager.updateRoom(room);
            fail("negative roomNumber not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            room = manager.getRoom(roomId);
            room.setRoomNumber(0);
            manager.updateRoom(room);
            fail("zero roomNumber not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            room = manager.getRoom(roomId);
            room.setCapacity(-1);
            manager.updateRoom(room);
            fail("negative capacity not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            room = manager.getRoom(roomId);
            room.setCapacity(0);
            manager.updateRoom(room);
            fail("zero capacity not detected");
        } catch (IllegalArgumentException ex) {
            //OK
        }

    }

    /**
     * Test of deleteRoom method, of class RoomManagerImpl.
     */
    @Test
    public void testDeleteRoom() {
        Room r1 = createAndSetRoom(12, 13, 6);
        Room r2 = createAndSetRoom(18, 19, 100);
        manager.createRoom(r1);
        manager.createRoom(r2);

        assertNotNull(manager.getRoom(r1.getId()));
        assertNotNull(manager.getRoom(r2.getId()));

        manager.deleteRoom(r1);

        assertNull(manager.getRoom(r1.getId()));
        assertNotNull(manager.getRoom(r2.getId()));
    }

    /**
     * Test of listAllRooms method, of class RoomManagerImpl.
     */
    @Test
    public void testListAllRooms() {
        assertTrue(manager.listAllRooms().isEmpty());

        Room r1 = createAndSetRoom(23, 44, 5);
        Room r2 = createAndSetRoom(12, 4, 1);

        manager.createRoom(r1);
        manager.createRoom(r2);

        Set<Room> expected = new HashSet<>(Arrays.asList(r1, r2));
        Set<Room> actual = new HashSet<>(manager.listAllRooms());

        assertEquals("saved and retrieved rooms differ", expected, actual);
    }
    
    private Room createAndSetRoom(int floor, int roomNumber, int capacity) {
        Room result = new Room();
        result.setFloor(floor);
        result.setRoomNumber(roomNumber);
        result.setCapacity(capacity);
        return result;
    }

}
