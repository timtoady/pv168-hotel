package cz.muni.fi.pv168.hotel.backend;

import java.util.Collection;

/**
 *
 * @author sahib
 */
public interface RoomManager {

    /**
     *
     * @param r
     */
    void createRoom(Room r);

    /**
     *
     * @param r
     */
    void updateRoom(Room r);

    /**
     *
     * @param r
     */
    void deleteRoom(Room r);

    /**
     *
     * @param id
     * @return
     */
    Room getRoom(Long id);

    /**
     *
     * @return
     */
    Collection<Room> listAllRooms();

}
