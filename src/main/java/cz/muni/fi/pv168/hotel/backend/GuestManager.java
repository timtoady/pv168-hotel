package cz.muni.fi.pv168.hotel.backend;

import cz.muni.fi.pv168.common.ServiceFailureException;
import java.util.Collection;

/**
 *
 * @author sahib
 */
public interface GuestManager {

    /**
     *
     * @param guest
     */
    void createGuest(Guest guest) throws ServiceFailureException;

    /**
     *
     * @param guest
     */
    void updateGuest(Guest guest) throws ServiceFailureException;

    /**
     *
     * @param guest
     */
    void deleteGuest(Guest guest) throws ServiceFailureException;

    /**
     *
     * @param id
     * @return
     */
    Guest getGuest(Long id) throws ServiceFailureException;
    
    /**
     *
     * @param name
     * @return
     * @throws ServiceFailureException
     */
    public Collection<Guest> getGuestByName(String name) throws ServiceFailureException;

    /**
     *
     * @return
     */
    Collection<Guest> listAllGuests() throws ServiceFailureException;

}
