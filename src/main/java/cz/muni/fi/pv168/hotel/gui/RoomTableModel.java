/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pv168.hotel.gui;

import cz.muni.fi.pv168.hotel.backend.Room;
import cz.muni.fi.pv168.hotel.backend.RoomManager;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author sahib
 */
public class RoomTableModel extends AbstractTableModel {
    
    private static final Logger LOGGER = Logger.getLogger(RoomTableModel.class.getName());
    private RoomManager roomManager;
    private List<Room> rooms = new ArrayList<>();
    private static enum COLUMNS {
        ID, FLOOR, NUMBER, CAPACITY
    }
    
    public void setRoomManager(RoomManager roomManager) {
        this.roomManager = roomManager;
    }
 
    @Override
    public int getRowCount() {
        return rooms.size();
    }
 
    @Override
    public int getColumnCount() {
        return COLUMNS.values().length;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
	switch (columnIndex) {
	    case 0:
		return Long.class;
	    case 1:
	    case 2:
	    case 3:
		return Long.class;
	    default:
		throw new IllegalArgumentException("columnIndex");
	}
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Room room = rooms.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return room.getId();
            case 1:
                return room.getFloor();
            case 2:
                return room.getRoomNumber();
            case 3:
                return room.getCapacity();
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    public void addRoom(Room room) {
	rooms.add(room);
	fireTableDataChanged();
    }
    
    public void removeRoom(Room room) {
	rooms.remove(room);
	fireTableDataChanged();
    }
    
    public void clear() {
	rooms.clear();
        fireTableDataChanged();
    }
    
     public List<Room> getAllRooms() {
	return rooms;
    }

    
    @Override
    public String getColumnName(int columnIndex) {
	switch (COLUMNS.values()[columnIndex]) {
	    case ID:
		return java.util.ResourceBundle.getBundle("cz/muni/fi/pv168/hotel/gui/Bundle").getString("rooms_table_id");
	    case FLOOR:
		return java.util.ResourceBundle.getBundle("cz/muni/fi/pv168/hotel/gui/Bundle").getString("rooms_table_floor");
	    case NUMBER:
		return java.util.ResourceBundle.getBundle("cz/muni/fi/pv168/hotel/gui/Bundle").getString("rooms_table_number");
	    case CAPACITY:
		return java.util.ResourceBundle.getBundle("cz/muni/fi/pv168/hotel/gui/Bundle").getString("rooms_table_capacity");
	    default:
		throw new IllegalArgumentException("columnIndex");
	}
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	Room room = rooms.get(rowIndex);
	switch (COLUMNS.values()[columnIndex]) {
	    case FLOOR:
		room.setFloor((int) aValue);
		break;
	    case NUMBER:
		room.setRoomNumber((int) aValue);
		break;
	    case CAPACITY:
		room.setCapacity((int) aValue);
		break;
	    default:
		throw new IllegalArgumentException("columnIndex");
	}
        try {
            roomManager.updateRoom(room);
            fireTableDataChanged();
        } catch (Exception ex) {
            String msg = "User request failed";
            LOGGER.log(Level.INFO, msg);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
	switch (columnIndex) {
	    case 1:
	    case 2:
            case 3:
		return true;
	    case 0:
		return false;
	    default:
		throw new IllegalArgumentException("columnIndex");
	}
    }

}