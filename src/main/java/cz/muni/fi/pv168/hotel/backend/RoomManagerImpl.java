package cz.muni.fi.pv168.hotel.backend;

import static cz.muni.fi.pv168.common.DBUtils.getId;
import cz.muni.fi.pv168.common.ServiceFailureException;
import java.util.Collection;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sahib, mik
 */
public class RoomManagerImpl implements RoomManager {

    private final DataSource dataSource;

    public RoomManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void createRoom(Room r) throws ServiceFailureException {
        validate(r);
        if (r.getId() != null) {
            throw new IllegalArgumentException("Room id should not be assigned "
                    + "prior saving " + r);
        }

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "INSERT INTO room (floor,roomNumber,capacity) VALUES "
                        + "(?,?,?)",
                        Statement.RETURN_GENERATED_KEYS)) {

            st.setInt(1, r.getFloor());
            st.setInt(2, r.getRoomNumber());
            st.setInt(3, r.getCapacity());
            int numOfNewRows = st.executeUpdate();
            if (numOfNewRows != 1) {
                throw new ServiceFailureException("Error: More rows ("
                        + numOfNewRows + ") inserted when trying to insert new "
                        + "room " + r);
            }

            ResultSet keyRS = st.getGeneratedKeys();
            r.setId(getId(keyRS));

        } catch (SQLException sqlEx) {
            throw new ServiceFailureException("Error when inserting room " + r,
                    sqlEx);
        }
    }

    static void validate(Room r) {
        if (r == null) {
            throw new IllegalArgumentException("Room should not be null");
        }
        if (r.getFloor() < 0) {
            throw new IllegalArgumentException("Room floor must not be "
                    + "negative: " + r);
        }
        if (r.getRoomNumber() < 1) {
            throw new IllegalArgumentException("Room number must be greater "
                    + "than zero: " + r);
        }
        if (r.getCapacity() < 1) {
            throw new IllegalArgumentException("Room capacity must be greater "
                    + "than zero: " + r);
        }
    }

    @Override
    public void updateRoom(Room r) throws ServiceFailureException {
        validate(r);
        if (r.getId() == null) {
            throw new IllegalArgumentException("Room id should "
                    + " be assigned prior updating: " + r);
        }

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "UPDATE room SET floor = ?, roomNumber = ?, "
                                + "capacity = ? WHERE id = ?")) {

            st.setInt(1, r.getFloor());
            st.setInt(2, r.getRoomNumber());
            st.setInt(3, r.getCapacity());
            st.setLong(4, r.getId());

            int count = st.executeUpdate();
            if (count == 0) {
                throw new ServiceFailureException("Room " + r + " was not "
                        + "found in database.");
            } else if (count != 1) {
                throw new ServiceFailureException("Invalid updated rows count "
                        + "detected: " + count + "(one row should be updated)."
                );
            }
        } catch (SQLException sqlEx) {
            throw new ServiceFailureException(
                    "Error when updating room " + r, sqlEx);
        }
    }

    @Override
    public void deleteRoom(Room r) throws ServiceFailureException {
        if (r == null) {
            throw new IllegalArgumentException("Room to be deleted is null.");
        }
        if (r.getId() == null) {
            throw new IllegalArgumentException("Id of room to be deleted is "
                    + "null.");
        }
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "DELETE FROM room WHERE id = ?")) {

            st.setLong(1, r.getId());

            int count = st.executeUpdate();
            if (count == 0) {
                throw new ServiceFailureException("Room " + r + " was not "
                        + "found in database.");
            } else if (count != 1) {
                throw new ServiceFailureException("Invalid deleted rows count "
                        + "detected: " + count + "(one row should be updated)."
                );
            }
        } catch (SQLException sqlEx) {
            throw new ServiceFailureException("Error when updating room "
                    + r, sqlEx);
        }
    }

    @Override
    public Room getRoom(Long id) throws ServiceFailureException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT id,floor,roomNumber,capacity FROM room WHERE "
                        + "id = ?")) {
            st.setLong(1, id);
            return executeQueryForSingleRoom(st);
        } catch (SQLException sqlEx) {
            throw new ServiceFailureException(
                    "Error when retrieving room with id " + id, sqlEx);
        }
    }

    private static Room resultSetToRoom(ResultSet rs) throws SQLException {
        Room r = new Room();
        r.setId(rs.getLong("id"));
        r.setFloor(rs.getInt("floor"));
        r.setRoomNumber(rs.getInt("roomNumber"));
        r.setCapacity(rs.getInt("capacity"));
        return r;
    }
    
    static Room executeQueryForSingleRoom(PreparedStatement st) 
            throws SQLException, ServiceFailureException {
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            Room result = resultSetToRoom(rs);                
            if (rs.next()) {
                throw new ServiceFailureException(
                        "Internal integrity error: more rooms with the same id "
                                + "found!");
            }
            return result;
        } else {
            return null;
        }
    }

    static List<Room> executeQueryForMultipleRooms(PreparedStatement st) 
            throws SQLException {
        ResultSet rs = st.executeQuery();
        List<Room> result = new ArrayList<>();
        while (rs.next()) {
            result.add(resultSetToRoom(rs));
        }
        return result;
    }

    @Override
    public Collection<Room> listAllRooms() throws ServiceFailureException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT id,floor,roomNumber,capacity FROM room")) {
            return executeQueryForMultipleRooms(st);
        } catch (SQLException sqlEx) {
            throw new ServiceFailureException(
                    "Error when retrieving all rooms", sqlEx);
        }
    }

}
