package cz.muni.fi.pv168.hotel.backend;

import cz.muni.fi.pv168.common.IllegalEntityException;
import cz.muni.fi.pv168.common.DBUtils;
import cz.muni.fi.pv168.common.ServiceFailureException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Clock;
import java.time.LocalDate;
import java.util.Collection;
import javax.sql.DataSource;

/**
 *
 * @author sahib, mik
 */
public class HotelManagerImpl implements HotelManager {

    private final DataSource dataSource;
    private Clock clock;

    public HotelManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public HotelManagerImpl(DataSource dataSource, Clock clock) {
        this.dataSource = dataSource;
        this.clock = clock;
    }

    @Override
    public void checkInGuest(Guest guest, Room room) {
        GuestManagerImpl.validate(guest);
        RoomManagerImpl.validate(room);
        if (guest.getId() == null) {
            throw new IllegalArgumentException("guest id must be set");
        }
        if (room.getId() == null) {
            throw new IllegalArgumentException("room id must be set");
        }
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            
            conn.setAutoCommit(false);
            checkIfRoomHasSpace(conn, room);
            checkIfGuestCheckedElsewhere(conn, guest);
            st = conn.prepareStatement("INSERT INTO accommodation (guestid, roomid, startDate) VALUES (?,?,?)");
//            st = conn.prepareStatement("INSERT INTO accommodation (guestid, roomid, startDate) "
//                    + "SELECT ?, ?, ? FROM accommodation "
//                    + "WHERE (SELECT COUNT(*) FROM accommodation WHERE guestid = ? AND endDate IS NULL) = 0 "
//                    + "FETCH FIRST ROW ONLY");
//            st.setLong(4, room.getId());
            st.setLong(1, guest.getId());
            st.setLong(2, room.getId());
            LocalDate today;
            if (clock != null) {
                today = LocalDate.now(clock);
            } else {
                today = LocalDate.now();
            }
            st.setDate(3, toSqlDate(today));
            int count = st.executeUpdate();
            if (count == 0) {
                throw new IllegalEntityException("Guest " + guest + " not found");
            }
            DBUtils.checkUpdatesCount(count, guest, true);
            conn.commit();
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when when checkin guest" + guest + " " + room, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    private static Date toSqlDate(LocalDate localDate) {
        return localDate == null ? null : Date.valueOf(localDate);
    }

    private static void checkIfRoomHasSpace(Connection conn, Room room) throws IllegalEntityException, SQLException {
        PreparedStatement checkSt = null;
        try {
            checkSt = conn.prepareStatement(
                    "SELECT capacity, COUNT(roomid) as guestCount "
                    + "FROM room LEFT JOIN accommodation ON room.id=roomid "
                    + "WHERE endDate IS NULL AND room.id = ? "
                    + "GROUP BY room.id,capacity");
//            System.out.println("OK!.");
            checkSt.setLong(1, room.getId());
            ResultSet rs = checkSt.executeQuery();
            if (rs.next()) {
                if (rs.getInt("capacity") <= rs.getInt("guestCount")) {
                    throw new IllegalEntityException("Room " + room + " is already full");
                }
            } else {
                throw new IllegalEntityException("Room " + room + " does not exist in the database");
            }
        } finally {
            DBUtils.closeQuietly(null, checkSt);
        }
    }
    
    private static void checkIfGuestCheckedElsewhere(Connection conn, Guest guest) throws IllegalEntityException, SQLException {
        PreparedStatement checkSt = null;
        try {
            checkSt = conn.prepareStatement(
                    "SELECT COUNT(*) AS roomCount FROM accommodation "
                    + "WHERE endDate IS NULL AND guestid = ? ");
            checkSt.setLong(1, guest.getId());
            ResultSet rs = checkSt.executeQuery();
            if (rs.next()) {
                if (rs.getInt("roomCount") != 0) {
                    throw new IllegalEntityException("Guest " + guest + " already checked in");
                }
            } else {
                throw new IllegalEntityException("Guest " + guest + " does not exist in the database");
            }
        } finally {
            DBUtils.closeQuietly(null, checkSt);
        }
    }

    @Override
    public void checkOutGuest(Guest guest) {
        GuestManagerImpl.validate(guest);
        if (guest.getId() == null) {
            throw new IllegalArgumentException("guest id should be already set");
        }
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "UPDATE accommodation SET endDate = ? WHERE guestid = ? AND endDate IS NULL")) {
            LocalDate today;
            if (clock != null) {
                today = LocalDate.now(clock);
            } else {
                today = LocalDate.now();
            }
            st.setDate(1, toSqlDate(today));
            st.setLong(2, guest.getId());
            int count = st.executeUpdate();
            if (count != 1) {
                throw new ServiceFailureException("Invalid updated rows count detected (one row should be updated): " + count);
            }
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when checking out guest " + guest, ex);
        }
    }

    @Override
    public Room findRoomWithGuest(Guest guest) {
        GuestManagerImpl.validate(guest);
        if (guest.getId() == null) {
            throw new IllegalArgumentException("guest id should be already set");
        }
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT room.id, floor, roomNumber, capacity "
                        + "FROM room LEFT JOIN accommodation ON room.id=roomid "
                        + "WHERE endDate IS NULL AND guestid = ?")) {
            st.setLong(1, guest.getId());
            return RoomManagerImpl.executeQueryForSingleRoom(st);
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when finding room with guest " + guest, ex);
        }
    }

    @Override
    public Collection<Guest> listGuestsInRoom(Room room) {
        RoomManagerImpl.validate(room);
        if (room.getId() == null) {
            throw new IllegalArgumentException("room id should be already set");
        }
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT guest.id, name, address, cardid "
                        + "FROM guest JOIN accommodation ON guest.id=guestid "
                        + "WHERE endDate IS NULL AND roomid = ?")) {
            st.setLong(1, room.getId());
            return GuestManagerImpl.executeQueryForMultipleGuests(st);
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when listing guests in room " + room, ex);
        }
    }

    @Override
    public Collection<Room> listEmptyRooms() {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT room.id, floor, roomNumber, capacity "
                        + "FROM room LEFT JOIN accommodation ON room.id=roomid "
                        + "WHERE endDate IS NULL "
                        + "GROUP BY room.id, floor, roomNumber, capacity "
                        + "HAVING COUNT(roomid) = 0")) {
            return RoomManagerImpl.executeQueryForMultipleRooms(st);
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when listing empty rooms", ex);
        }
    }

    @Override
    public Collection<Room> listRoomsWithSomeFreeSpace() {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT room.id, floor, roomNumber, capacity "
                        + "FROM room LEFT JOIN accommodation ON room.id=roomid "
                        + "WHERE endDate IS NULL "
                        + "GROUP BY room.id, floor, roomNumber, capacity "
                        + "HAVING COUNT(roomid) < capacity")) {
            return RoomManagerImpl.executeQueryForMultipleRooms(st);
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when listing rooms with some space", ex);
        }
    }

}
