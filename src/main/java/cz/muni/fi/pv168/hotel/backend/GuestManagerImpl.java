package cz.muni.fi.pv168.hotel.backend;


import cz.muni.fi.pv168.common.ServiceFailureException;
import static cz.muni.fi.pv168.common.DBUtils.getId;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Miroslav Machala
 */
public class GuestManagerImpl implements GuestManager {
    
    private final DataSource dataSource;

    public GuestManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void createGuest(Guest guest) throws ServiceFailureException {
        validate(guest);
        if (guest.getId() != null) {
            throw new IllegalArgumentException("guest id should not be assigned prior saving");
        }
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "INSERT INTO guest (name, address, cardid) VALUES (?,?,?)",
                        Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, guest.getName());
            st.setString(2, guest.getAddress());
            st.setString(3, guest.getIdCard());
            int addedRows = st.executeUpdate();
            if (addedRows != 1) {
                throw new ServiceFailureException("Internal Error: More rows ("
                        + addedRows + ") inserted when trying to insert guest " + guest);
            }
            ResultSet keyRS = st.getGeneratedKeys();
            guest.setId(getId(keyRS));
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when inserting guest " + guest, ex);
        }

    }

    static void validate(Guest g) throws IllegalArgumentException {
        if (g == null) {
            throw new IllegalArgumentException("guest should not be null");
        }
        if (g.getName() == null) {
            throw new IllegalArgumentException("guest name should not be null");
        }
        if (g.getAddress() == null) {
            throw new IllegalArgumentException("guest adress should not be null");
        }
        if (g.getIdCard() == null) {
            throw new IllegalArgumentException("guest idCard should not be null");
        }
//        mozna jeste testovat delku stringu, kvuli databazi?
    }

    @Override
    public void updateGuest(Guest guest) throws ServiceFailureException {
        validate(guest);
        if (guest.getId() == null) {
            throw new IllegalArgumentException("guest id should be already set");
        }
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "UPDATE guest SET name = ?, address = ?, cardid = ? WHERE id = ?")) {
            st.setString(1, guest.getName());
            st.setString(2, guest.getAddress());
            st.setString(3, guest.getIdCard());
            st.setLong(4, guest.getId());
            int count = st.executeUpdate();
            if (count == 0) {
                throw new ServiceFailureException("Guest " + guest + " was not found in database!");
            } else if (count != 1) {
                throw new ServiceFailureException("Invalid updated rows count detected (one row should be updated): " + count);
            }
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when updating guest " + guest, ex);
        }

    }

    @Override
    public void deleteGuest(Guest guest) throws ServiceFailureException {
        if (guest == null) {
            throw new IllegalArgumentException("guest is null");
        }
        if (guest.getId() == null) {
            throw new IllegalArgumentException("guest id is null");
        }
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement("DELETE FROM guest WHERE id = ?")) {
            st.setLong(1, guest.getId());
            int count = st.executeUpdate();
            if (count == 0) {
                throw new ServiceFailureException("Guest " + guest + " was not found in database!");
            } else if (count != 1) {
                throw new ServiceFailureException("Invalid deleted rows count detected (one row should be updated): " + count);
            }
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when updating guest " + guest, ex);
        }

    }

    @Override
    public Guest getGuest(Long id) throws ServiceFailureException {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement("SELECT * FROM guest WHERE id = ?")) {
            st.setLong(1, id);
            return executeQueryForSingleGuest(st);
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when retrieving guest with id " + id, ex);
        }
    }
    
    @Override
    public Collection<Guest> getGuestByName(String name) throws ServiceFailureException {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement("SELECT * FROM guest WHERE name LIKE ?")) {
            st.setString(1, "%" + name + "%");
            return executeQueryForMultipleGuests(st);
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when retrieving guest with name " + name, ex);
        }
    }

    private static Guest resultSetToGuest(ResultSet rs) throws SQLException {
        Guest guest = new Guest();
        guest.setId(rs.getLong("id"));
        guest.setName(rs.getString("name"));
        guest.setAddress(rs.getString("address"));
        guest.setIdCard(rs.getString("cardid"));
        return guest;
    }
    
    static Guest executeQueryForSingleGuest(PreparedStatement st) throws SQLException, ServiceFailureException {
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            Guest result = resultSetToGuest(rs);                
            if (rs.next()) {
                throw new ServiceFailureException(
                        "Internal integrity error: more guests with the same id found!");
            }
            return result;
        } else {
            return null;
        }
    }

    static List<Guest> executeQueryForMultipleGuests(PreparedStatement st) throws SQLException {
        ResultSet rs = st.executeQuery();
        List<Guest> result = new ArrayList<>();
        while (rs.next()) {
            result.add(resultSetToGuest(rs));
        }
        return result;
    }

    @Override
    public Collection<Guest> listAllGuests() throws ServiceFailureException {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement("SELECT * FROM guest")) {
            return executeQueryForMultipleGuests(st);
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error when retrieving all guests", ex);
        }
    }

}
