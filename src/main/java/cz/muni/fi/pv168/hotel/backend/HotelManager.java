package cz.muni.fi.pv168.hotel.backend;

import java.util.Collection;

/**
 *
 * @author sahib
 */
public interface HotelManager {

    /**
     *
     * @param guest
     * @param room
     */
    void checkInGuest(Guest guest, Room room);

//    /**
//     *
//     * @param guest
//     * @param room
//     */
//    void moveGuest(Guest guest, Room room);
    
    /**
     *
     * @param guest
     */
    void checkOutGuest(Guest guest);

    /**
     *
     * @param guest
     * @return
     */
    Room findRoomWithGuest(Guest guest);

    /**
     *
     * @param room
     * @return
     */
    Collection<Guest> listGuestsInRoom(Room room);

    /**
     *
     * @return
     */
    Collection<Room> listEmptyRooms();
    
    /**
     *
     * @return
     */
    Collection<Room> listRoomsWithSomeFreeSpace();

}
