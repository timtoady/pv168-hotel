package cz.muni.fi.pv168.hotel.backend;

import java.util.Objects;

/**
 *
 * @author sahib, mik
 */
public class Room {

    private Long id;
    private int floor;
    private int roomNumber;
    private int capacity;

    public Room() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public int hashCode() {
        /*
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.id);
        hash = 19 * hash + this.floor;
        hash = 19 * hash + this.roomNumber;
        hash = 19 * hash + this.capacity;
        return hash;
         */
        return Objects.hashCode(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Room other = (Room) obj;
        /*
        if (this.floor != other.floor) {
            return false;
        }
        if (this.roomNumber != other.roomNumber) {
            return false;
        }
        if (this.capacity != other.capacity) {
            return false;
        }
         */
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Room{" + "id=" + id + ", floor=" + floor + ", roomNumber="
                + roomNumber + ", capacity=" + capacity + '}';
    }

}
