/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pv168.hotel.gui;

import cz.muni.fi.pv168.hotel.backend.Guest;
import cz.muni.fi.pv168.hotel.backend.GuestManager;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author sahib
 */
public class GuestTableModel extends AbstractTableModel {
    
    private static final Logger LOGGER = Logger.getLogger(GuestTableModel.class.getName());
    private GuestManager guestManager;
    private List<Guest> guests = new ArrayList<>();
    private static enum COLUMNS {
        ID, NAME, ADDRESS, CARDID
    }
    
    public void setGuestManager(GuestManager guestManager) {
        this.guestManager = guestManager;
    }
 
    @Override
    public int getRowCount() {
        return guests.size();
    }
 
    @Override
    public int getColumnCount() {
        return COLUMNS.values().length;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
	switch (columnIndex) {
	    case 0:
		return Long.class;
	    case 1:
	    case 2:
	    case 3:
		return String.class;
	    default:
		throw new IllegalArgumentException("columnIndex");
	}
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Guest guest = guests.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return guest.getId();
            case 1:
                return guest.getName();
            case 2:
                return guest.getAddress();
            case 3:
                return guest.getIdCard();
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    public void addGuest(Guest guest) {
	guests.add(guest);
	fireTableDataChanged();
    }
    
    public void removeGuest(Guest guest) {
	guests.remove(guest);
	fireTableDataChanged();
    }
    
    public void clear() {
	guests.clear();
        fireTableDataChanged();
    }
    
     public List<Guest> getAllGuests() {
	return guests;
    }

    
    @Override
    public String getColumnName(int columnIndex) {
	switch (COLUMNS.values()[columnIndex]) {
	    case ID:
		return java.util.ResourceBundle.getBundle("cz/muni/fi/pv168/hotel/gui/Bundle").getString("guests_table_id");
	    case NAME:
		return java.util.ResourceBundle.getBundle("cz/muni/fi/pv168/hotel/gui/Bundle").getString("guests_table_name");
	    case ADDRESS:
		return java.util.ResourceBundle.getBundle("cz/muni/fi/pv168/hotel/gui/Bundle").getString("guests_table_address");
	    case CARDID:
		return java.util.ResourceBundle.getBundle("cz/muni/fi/pv168/hotel/gui/Bundle").getString("guests_table_cardid");
	    default:
		throw new IllegalArgumentException("columnIndex");
	}
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	Guest guest = guests.get(rowIndex);
	switch (COLUMNS.values()[columnIndex]) {
	    case NAME:
		guest.setName((String) aValue);
		break;
	    case ADDRESS:
		guest.setAddress((String) aValue);
		break;
	    case CARDID:
		guest.setIdCard((String) aValue);
		break;
	    default:
		throw new IllegalArgumentException("columnIndex");
	}
        try {
            guestManager.updateGuest(guest);
            fireTableDataChanged();
        } catch (Exception ex) {
            String msg = "User request failed";
            LOGGER.log(Level.INFO, msg);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
	switch (columnIndex) {
	    case 1:
	    case 2:
            case 3:
		return true;
	    case 0:
		return false;
	    default:
		throw new IllegalArgumentException("columnIndex");
	}
    }

}